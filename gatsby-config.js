const dotenv = require('dotenv')

if(process.env.NODE_ENV === 'production'){
  dotenv.config()
}
module.exports = {
  pathPrefix: `/blog`,
  siteMetadata: {
    title: `Doman Blog`,
    author: `Doman`,
    description: `A blog to develop`,
    siteUrl: 'https://d0man.gitlab.io/blog/',
    root: '/blog',
    social: {
      linkedin: {
        address: `https://www.linkedin.com/in/jakubdoma%C5%84ski/`,
        icon: ''
      },
      github: {
        address: `https://github.com/D0man/`,
        icon: ''
      }
    },
  },
  plugins: [
     `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-155215623-1`,
      },
    },
    {
      resolve: "gatsby-plugin-feed",
      options: {
        setup(ref) {
          const metaInfo = ref.query.site.siteMetadata

          metaInfo.generator = "GatsbyJS test"
          return metaInfo
        },
        query: `
        {
          site {
            siteMetadata {
              title
              description
              siteUrl
              site_url: siteUrl
            }
          }
        }
  `,
        feeds: [
          {
            serialize(value) {
              const rssMetadata = value.query.site.siteMetadata
              return value.query.allContentfulPost.edges.map(edge => ({
                description: edge.node.subtitle,
                date: edge.node.time,
                url: rssMetadata.siteUrl + edge.node.slug,
                guid: rssMetadata.siteUrl + edge.node.slug,

              }))
            },
            query: `
            {
              allContentfulPost {
                edges {
                  node {
                    id
                    slug
                    time
                    subtitle
                  }
                }
              }
            }
      `,
            output: "/fees.xml",
            title: "Doman RSS feed",
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Doman Blog`,
        short_name: `Dblog`,
        start_url: `/blog`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `content/assets/gatsby-icon.png`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography.js`,
      },
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: `ae21snxqkj0c`,
        accessToken: '5nba3rhp2aqe5imJiDVWip6FQXmru2S59UjHF5ox55I',
      },
    },
  ],
}
