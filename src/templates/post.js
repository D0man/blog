import React from "react"
import { Link, graphql } from "gatsby"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm, scale } from "../utils/typography"
import { BLOCKS, MARKS } from "@contentful/rich-text-types"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

class BlogPostTemplate extends React.Component {

  render() {
     const Bold = ({ children }) => <span className="bold">{children}</span>
     const Text = ({ children }) => <p className="align-center">{children}</p>
     const options = {
       renderMark: {
         [MARKS.BOLD]: text => <Bold>{text}</Bold>,
       },
       renderNode: {
         [BLOCKS.PARAGRAPH]: (node, children) => <Text>{children}</Text>,
       },
     }
    const post = this.props.data.contentfulPost
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext
    const richText = documentToReactComponents(post.content.json, options)
    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title={post.title}
          description={post.subtitle}
        />
        <article>
          <header>
            <h1
              style={{
                marginTop: rhythm(1),
                marginBottom: 0,
              }}
            >
              {post.title}
            </h1>
            <p
              style={{
                ...scale(-1 / 5),
                display: `block`,
                marginBottom: rhythm(1),
              }}
            >
              {post.time}
            </p>
          </header>
          <section>
          {richText}</section>
          <hr
            style={{
              marginBottom: rhythm(1),
            }}
          />
          <footer>
          </footer>
        </article>

        <nav>
          <ul
            style={{
              display: `flex`,
              flexWrap: `wrap`,
              justifyContent: `space-between`,
              listStyle: `none`,
              padding: 0,
            }}
          >
            <li>
              {previous && (
                <Link to={previous.slug} rel="prev">
                  ← {previous.title}
                </Link>
              )}
            </li>
            <li>
              {next && (
                <Link to={next.slug} rel="next">
                  {next.title} →
                </Link>
              )}
            </li>
          </ul>
        </nav>
        <hr
          style={{
            marginBottom: rhythm(1),
          }}
        />
        <Bio />
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
         query BlogPostBySlug($slug: String!) {
           site {
             siteMetadata {
               title
               author,
               siteUrl,
             }
           }
           contentfulPost(slug: { eq: $slug }) {
             title
             time
             subtitle
             author
             content {
               json
             }
           }
         }
       `
