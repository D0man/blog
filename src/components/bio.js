/**
 * Bio component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Image from "gatsby-image"

import { rhythm } from "../utils/typography"

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioQuery {
      avatar: file(absolutePath: { regex: "/profile-pic.jpg/" }) {
        childImageSharp {
          fixed(width: 75, height: 99) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      site {
        siteMetadata {
          author
          social {
            linkedin {
              address
            }
            github {
              address
            }
          }
        }
      }
    }
  `)

  const { author, social } = data.site.siteMetadata
  return (
    <div
      style={{
        display: `flex`,
      }}
    >
      <Image
        fixed={data.avatar.childImageSharp.fixed}
        objectFit="cover"
        objectPosition="50% 50%"
        alt={author}
        imgStyle={{
          marginRight: rhythm(1),
          border: '1px solid #ccc'}}
  />
      <div className="social">
        <p className="social__paragraph"> Blog stworzony przez <strong>{author}</strong>, programista z Warszawy.</p>
        <p className="social__paragraph">Możesz mnie znaleźć na:</p>
        <ul className="social__list">
          <li className="social__item"><a href={social.linkedin.address}>
          linkedin
          </a></li>
          <li className="social__item"><a href={social.github.address}>
            github
          </a></li>
        </ul>
      </div>
    </div>
  )
}

export default Bio
