import React from "react"

class Cookie extends React.Component {
  constructor(props){
    super(props)
    this.state = {show: true};
  }
  componentDidMount() {
    if (localStorage.getItem('sawCookies')) {
        this.setState({show: false})
    }
  }
  handleClick = () => {
    localStorage.setItem('sawCookies', 'True')
    this.setState({ show: false })
  }
  render() {
    return (
      this.state.show &&
      <div className="cookie">
        Blog wykorzystuje pliki cookie zgodnie z ustawieniami Twojej przeglądarki internetowej. Szczegółowe informacje na temat cookie znajdziesz gdzieś w internatach.<button className="cookie__close" onClick={this.handleClick}>×</button>
      </div>
    )
  }
}

export default Cookie;