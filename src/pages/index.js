import React from "react"
import { Link, graphql } from "gatsby"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import './homepage.scss'

class BlogIndex extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = `Blog Domana`
    const posts = data.allContentfulPost.edges
  
    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title="Doman blog" />
        <div className="posts">
        {posts.map(({ node }) => {
          const title = node.title || node.slug
          return (
            <article className="post" key={node.slug}>
              <header>
                <h3 className="post__title">
                  <Link to={node.slug}>
                    {title}
                  </Link>
                </h3>
                <time dateTime={node.time}>{node.time}</time>
              </header>
              <section>
                <p>{node.subtitle}</p>
              </section>
              <footer>
                {node.tags.data.map(tag=><div key={tag} className="tag">{tag}</div>)}
              </footer>
            </article>
          )
        })}
        </div>
        <Bio />
      </Layout>
      
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        siteUrl
      }
    }
    allContentfulPost {
      edges {
        node {
          slug
          title
          subtitle
          time
          author
          tags {
            data
          }
        }
      }
    }

  }
`
